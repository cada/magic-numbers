Gem::Specification.new do |s|
  s.name        = 'magic_numbers'
  s.version     = '0.0.0'
  s.date        = '2018-10-15'
  s.summary     = "Magic Numbers!"
  s.description = "An X number is said to be 'magic' when the square root of X exists and is a prime number."
  s.authors     = ["Claudia Farias"]
  s.email       = 'claudia@sorta.in'
  s.files       = ["lib/magic_numbers.rb", "lib/magic_numbers/magic.rb"]
  s.homepage    = ''
  s.license     = 'MIT'
end