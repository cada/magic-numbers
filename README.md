## B2W Challenge

B2W recruiting challenge.

### Environment

Ruby version 2.4.

I chose Ruby because I have more experience and feel comfortable in programming Ruby.

### Setup 

```
  $ bundle install
```

### How its works - Running 

You can run this script in 3 different ways from command line:

Option 1:

```
  $ irb -Ilib -rmagic_numbers
   > mn = MagicNumbers.new *[8, 27]
  => #<MagicNumbers:0x0055f3935a12e8 @start=8, @ending=27, @primes=[]>
   > mn.presto!
  => [9, 25]
  ...
```

Option 2: 
Running as script

```
  $ ruby -Ilib ./bin/magic_numbers "[[8,27], [49,49]]"
  # 3
```

Alternatively you can run in interactive mode:

```
  $ ruby -Ilib ./bin/magic_numbers -i
  # Magic Numbers! 
  # Enter with the interval list: [[8,27], [49,49]]
  # 3
```

Option 3:

You can install it as a gem

```
  $ gem build magic_numbers.gemspec
  $ gem install ./magic_numbers-0.0.0.gem
  $ irb
   > require 'magic_numbers'
  => true
```

To run tests you will need have Minitest installed: 

`$ gem install minitest` 

Then: 

`$ rake` 

#### The Challenge Description

##### Números mágicos

Um número X é dito "mágico" quando a raiz quadrada de X existe e é um número primo.

Escreva um programa que receba como entrada uma lista de intervalos [A,B] e retorne o
somatório da quantidade de números mágicos encontrados em cada intervalo. É garantido
que os números A e B serão inteiros positivos e que A será sempre menor ou igual que B.

Para a entrada: [[8,27], [49,49]]
Resultado: 3
Seriam os números 9 e 25 do primeiro intervalo e 49 do segundo.

Escolha a linguagem de programação que você desejar (só avisa pra gente o porquê da
escolha).
Além da solução, escreva também alguns testes automatizados (usando qualquer
framework / ferramenta conhecido(a) ou apenas outras funções / métodos).
Além da corretude e complexidade (de tempo e espaço) da solução, analisaremos também:
legibilidade (da solução e do teste) e a cobertura dos testes.