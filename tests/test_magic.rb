describe MagicNumbers::Magic do

  describe "#is_magic?" do
    it "must respond" do
      MagicNumbers::Magic.new(9).must_respond_to :is_magic?
    end
  end

  describe "#is_prime?" do
    it "must respond positively" do
      @magic = MagicNumbers::Magic.new(2)
      @magic.send(:is_prime?).must_be :==, true
    end
  end

  describe "#is_square?" do
    it "must respond positively" do
      @magic = MagicNumbers::Magic.new(9)
      @magic.send(:is_square?).must_be :==, true
    end
  end

  describe "when is a magic number" do

    before do
      @magic = MagicNumbers::Magic.new(9)
    end

    it "must respond positively" do
      @magic.is_magic?.must_be :==, true
    end
  end

  describe "when is not a magic number" do

    before do
      @magic = MagicNumbers::Magic.new(12)
    end

    it "must respond negatively" do
      @magic.is_magic?.must_be :==, false
    end
  end
end