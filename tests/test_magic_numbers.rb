describe MagicNumbers do

  before { @magic_numbers = MagicNumbers.new(8, 27) }
  
  describe "#presto" do
    it "must respond to" do
      @magic_numbers.must_respond_to :presto!
    end

    it "must fill hocus_pocus " do
      @magic_numbers.hocus_pocus.must_be_empty
      @magic_numbers.presto!
      @magic_numbers.hocus_pocus.wont_be_empty
    end
  end
  
  describe "#hocus_pocus" do
    it "must respond to" do
      @magic_numbers.must_respond_to :hocus_pocus
    end
  end
  
  describe ".interactive_mode!" do
    it "must respond to" do
      MagicNumbers.must_respond_to :interactive_mode!
    end

    it "must return an integer" do
      MagicNumbers.interactive_mode!([[8, 27], [49, 49]]).must_equal 3
    end
  end

  describe "with wrong arguments" do
    it "raise ArgumentError" do
      proc { MagicNumbers.new(9, 8) }.must_raise ArgumentError
    end
  end

end