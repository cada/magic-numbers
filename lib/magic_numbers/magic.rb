class MagicNumbers::Magic
  # MagicNumbers::Prime
  #
  # Example:
  #   >> MagicNumbers::Prime.new(13)
  #   => #<MagicNumbers::Magic:0x0055f39353aa48 @number=8, @prime=true>
  #
  # Parameters:
  #   number: (String || Integer)

  def initialize number
    @number = number.to_i
    @magic = is_magic?
  end

  def is_magic?
    self.is_square? && self.is_prime?
  end

  protected

    def is_prime?
      @prime = true
      squared = (Math.sqrt(@number).to_i)
      (2..Math.sqrt(squared).to_i).select do |root|
        if (squared % root).zero?
          @prime = false
          break
        end
      end

      return @prime
    end

    def is_square?
      (Math.sqrt(@number) % 1).zero?
    end

end
