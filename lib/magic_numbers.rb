class MagicNumbers
  
  attr_accessor :start, :ending, :hocus_pocus
  
  def initialize start, ending
    self.start = start.to_i
    self.ending = ending.to_i
    self.hocus_pocus = []

    if self.ending < self.start
      raise ArgumentError.new("The `ending` parameter must be equal or greater than `start`")
    end
  end
  
  def presto! 
    self.start.upto(self.ending) do |number|
      self.hocus_pocus.push(number) if self.is_magic?(number)
    end

    return self.hocus_pocus
  end

  def self.interactive_mode! intervals
    solution = []
    intervals.each do |interval|
      mn = self.new(*interval)
      solution.push(*mn.presto!)
    end
    return solution.size
  end
  
  protected
  
    def is_magic? number
      MagicNumbers::Magic.new(number).is_magic?
    end

end

require 'magic_numbers/magic'